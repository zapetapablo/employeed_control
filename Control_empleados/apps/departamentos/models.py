from django.db import models

class Departament(models.Model):
    name = models.CharField(("Nombre"), max_length=50)
    short_name = models.CharField(("Alias"), max_length=20, unique=True)
    active = models.BooleanField(("Anulado"), default = False)

    class Meta:
        verbose_name = "Departamento"
        verbose_name_plural = "Departamentos"
        ordering = ['-name']
        unique_together = ('name', 'short_name') #no permite que se duplique esta combinancion de atributos

    def __str__(self):
        return "{}".format(self.name)
        # return "{} - {} - {}".format(self.id, self.name, self.active)

