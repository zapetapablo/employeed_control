from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    TemplateView,
    ListView,
)
from django.views.generic.edit import FormView


from apps.personas.models import Empleado
from .models import Departament

from .forms import New_form_departamet


class departament_ListView(ListView):
    template_name = "departament/departament_list.html"
    model = Departament
    context_object_name = 'departaments'



class New_departament_view(FormView):
    form_class = New_form_departamet
    template_name = 'departament/new_departament.html'
    success_url = reverse_lazy('departament_app:success')

    def form_valid(self, form):

        print('************** stay in form **************')
        depart = Departament(
            name        = form.cleaned_data['departament'],
            short_name  = form.cleaned_data['short_name'],
        )
        depart.save()

        name = form.cleaned_data['name']
        last_name = form.cleaned_data['last_name']
        Empleado.objects.create(
            first_name = name,
            last_name = last_name,
            job = 1,
            departament = depart,
        )
        return super(New_departament_view, self).form_valid(form)


class Success_View(TemplateView):
    template_name = "departament/success.html"
