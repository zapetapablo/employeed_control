from django.contrib import admin
from django.urls import path

from . import views

app_name = 'departament_app'

urlpatterns = [

    path(
        'success/',
        views.Success_View.as_view(), 
        name='success',
        ),
    path(
        'departaments-list',
        views.departament_ListView.as_view(),
        name = "departament_list",
    ),
    path(
        'nuevo_departamento/',
        views.New_departament_view.as_view(),
        name='New_departament',
        ),
]
