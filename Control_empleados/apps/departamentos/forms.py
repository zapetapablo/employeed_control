from django  import forms

class New_form_departamet(forms.Form):
    name = forms.CharField(max_length=50)
    last_name  = forms.CharField(max_length=50)
    departament = forms.CharField(max_length=30)
    short_name = forms.CharField(max_length=20)
