# Generated by Django 3.0.3 on 2020-03-14 04:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personas', '0005_auto_20200307_2239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='empleado',
            name='avatar',
            field=models.ImageField(blank=True, null=True, upload_to='empleado', verbose_name='Imagen de referencia'),
        ),
    ]
