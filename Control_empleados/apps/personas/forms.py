from django import forms

from .models import Empleado

class Employeed_Form(forms.ModelForm):
    
    class Meta:
        model = Empleado
        fields = (
            'first_name',
            'last_name',
            'full_name',
            'job',
            'departament',
            'avatar',
            'skills',
        )
        widgets = {
            'skills': forms.CheckboxSelectMultiple()
        }