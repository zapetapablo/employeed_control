from django.db import models
from apps.departamentos.models import Departament

class Skill(models.Model):
    name = models.CharField(("Habilidades"), max_length=50, null = True)

    class Meta:
        verbose_name = ("Habilidad")
        verbose_name_plural = ("Habilidades")

    def __str__(self):
        return "{} - {}".format(self.id, self.name)


class Empleado(models.Model):
    """modelo para la tabla empelado"""
    first_name = models.CharField(("Nombres"), max_length=75)
    last_name = models.CharField(("Apellidos"), max_length=75)
    full_name = models.CharField(
        ("Nombres Completos"),
         max_length=150,
         blank=True,
         )
    job = models.CharField(("Trabajo"), max_length=1, choices=(
         ('0', 'Modelista'),
         ('1', 'Developer_Backend'),
         ('2', 'Developer_Javascript'),
         ('3', 'Tester_developer'),
         ('4', 'contador@'),
         ('5', 'Administradora'),
         ('6', 'Tecnico de redes'),
         ))
    departament = models.ForeignKey(Departament, on_delete=models.CASCADE)
    avatar = models.ImageField(("Imagen de referencia"), upload_to='empleado', blank = True, null = True)
    skills = models.ManyToManyField(Skill)

    
    class Meta:
        verbose_name = "Empleado"
        verbose_name_plural = "Empleados"
        ordering = ['job']

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)
    
        
