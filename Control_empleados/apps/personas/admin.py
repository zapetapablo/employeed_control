from django.contrib import admin
from .models import Empleado, Skill

admin.site.register(Skill)


class EmpleadoAdmin(admin.ModelAdmin):
    list_display = (
        'first_name',
        'last_name',
        'departament',
        'job',
        'full_name',
        'id',
        )
    def full_name(self, obj):
        print(obj)
        return "{} {}".format(obj.first_name, obj.last_name)

    search_fields = (
        'first_name',
    )
    list_filter = (
        'job',
        'skills'
        )
    filter_horizontal = (
        'skills',
        )

admin.site.register(Empleado, EmpleadoAdmin)