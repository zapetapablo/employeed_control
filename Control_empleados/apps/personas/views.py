from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    TemplateView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
    )
#models
from .models import Empleado
#Forms
from .forms import Employeed_Form

class home_view(TemplateView):
    """PAGINA DE INICIO"""
    template_name = "index.html"


class ListAllEmploys(ListView):
    template_name = "person/list_all_employees.html"
    ordering = "first_name"
    paginate_by = 4
    context_object_name = 'list'

    def get_queryset(self):
        principal_word = self.request.GET.get("kword", '')
        #icontains = busca coincidencias por ejemplo j encuentra a jorge en este caso lo encuentra
        #porque todos los nombres van a tener algun vacio (almenos en full name)
        list_result = Empleado.objects.filter(
            first_name__icontains = principal_word
        )
        return list_result


class Admin_Employs_listView(ListView):
    template_name = "person/Admin_employs.html"
    ordering = "first_name"
    paginate_by = 10
    context_object_name = 'employs'
    model = Empleado


class ListbyArea(ListView):
    """ List employ of a area"""
    template_name = "person/byarea.html"
    context_object_name = 'Employeed'

    def get_queryset(self):
        area = self.kwargs['shortname']
        employeed_list = Empleado.objects.filter(
        departament__short_name = area
        )
        return employeed_list

class listEmployskeywords(ListView):
    template_name = "person/keyword.html"
    context_object_name = 'employees'

    def get_queryset(self):
        # print('************')
        principal_word = self.request.GET.get("kword", '')
        # print ('palabra clave '+ principal_word)
        list_result = Empleado.objects.filter(
            first_name = principal_word
        )
        # print ('lista: ', listaresult)
        return list_result
    
class skillEmployees(ListView):
    template_name = "person/skills.html"
    context_object_name = 'skillss'

    def get_queryset(self):
        allskills = self.request.GET.get("pallskills", '3')
        empleado = Empleado.objects.get(id=allskills)
        return empleado.skills.all()
    
class Employeed_DetailView(DetailView):
    model = Empleado
    template_name = "person/detail_empleado.html"

    def get_context_data(self, **kwargs):
        context = super(Employeed_DetailView, self).get_context_data(**kwargs)
        
        empleado = Empleado.objects.get(id=self.kwargs['pk'])
        context['skills'] = empleado.skills.all()
        
        return context


class successView(TemplateView):
    template_name = "person/success.html"


class Employeed_CreateView(CreateView):
    template_name = "person/add_employeed.html"
    model = Empleado
    form_class = Employeed_Form
    success_url = reverse_lazy('person_app:Exito')

    # fields = [
    #     'first_name',
    #     'last_name',
    #     'job',
    #     'departament',
    #     'skills',
    #     'avatar',
    # ]  #forma de hacerlo uno a uno
    # fields = ('__all__')         #Todos los campos del modelo empleado

    # se comento fields porque de esa forma con el contexto al llamarlo genera un form 
    # por defecto pero en este caso estamos usando form_class que sirve para personalizar
    # los forms y se vean de una forma mas profesional

    def form_valid(self, form):
        #logica del codigo
        employeed = form.save(commit = False)
        employeed.full_name = employeed.first_name + '' +employeed.last_name
        employeed.save()
        return super(Employeed_CreateView, self).form_valid(form)

class employeed_UpdateView(UpdateView):
    model = Empleado
    template_name = "person/update_employeed.html"
    fields = [
        'first_name',
        'last_name',
        'job',
        'departament',
        'skills'
    ]  #forma de hacerlo uno a uno

    success_url = reverse_lazy('person_app:Admin_Employs')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        # print("**********POST METHOD**********")
        # print(request)
        # print("*******************************")
        # print(request.POST['last_name'])
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        # print("**********Form valid method**********")
        # print("*******************************")
        return super(employeed_UpdateView, self).form_valid(form)

class Employeed_DeleteView(DeleteView):
    model = Empleado
    template_name = "person/delete_employeed.html"
    success_url = reverse_lazy('person_app:Admin_Employs')
    context_object_name = 'Employeed'
