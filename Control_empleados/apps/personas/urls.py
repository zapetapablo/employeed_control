from django.contrib import admin
from django.urls import path

from . import views
app_name = "person_app"

urlpatterns = [

    path(
        '',
        views.home_view.as_view(),
        name = 'index'
        ),
    path(
        'all-employeed-list/',
    views.ListAllEmploys.as_view(),
    name = 'all_persons',
    ),
    path(
        'people_for_area/<shortname>/',
        views.ListbyArea.as_view(),
        name = 'departament-people'
        ),
    path('buscarempleado/', views.listEmployskeywords.as_view()),
    path('listarhabilidades/', views.skillEmployees.as_view()),
    path(
        'see_employeed/<pk>/',
        views.Employeed_DetailView.as_view(),
        name = 'detail_employeed'        
        ),
    path(
        'AgregarEmpleado/',
        views.Employeed_CreateView.as_view(),
        name = "register_new_employeed"
        ),
    
    path(
        'Hecho/',
        views.successView.as_view(), 
        name = 'Exito'
    ),
    path(
        'update/<pk>/',
        views.employeed_UpdateView.as_view(), 
        name = 'update_employeed',
    ),
     path(
        'BorrarEmpleado/<pk>/',
        views.Employeed_DeleteView.as_view(), 
        name = 'delete_employeed',
    ),
    path(
        'admin-employs/',
        views.Admin_Employs_listView.as_view(), 
        name = 'Admin_Employs',
    ),


]
