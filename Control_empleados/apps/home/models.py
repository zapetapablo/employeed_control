from django.db import models

# Create your models here.
class Proob(models.Model):
    title = models.CharField(('Titulo'), max_length=50)
    sub_title = models.CharField(('Subtitulo'), max_length=50)
    quanti = models.IntegerField(('Cantidad'))

    class Meta:
        verbose_name = ("Prueba")
        verbose_name_plural = ("Pruebas")

    def __str__(self):
        return ' {} - {}'.format(self.title, self.sub_title)
        