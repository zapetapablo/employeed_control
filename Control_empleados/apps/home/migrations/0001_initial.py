# Generated by Django 3.0.3 on 2020-02-23 05:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Proob',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='titulo')),
                ('sub_title', models.CharField(max_length=50, verbose_name='Subtitulo')),
                ('quanti', models.IntegerField(verbose_name='Cantidad')),
            ],
            options={
                'verbose_name': 'Proob',
                'verbose_name_plural': 'Proobs',
            },
        ),
    ]
