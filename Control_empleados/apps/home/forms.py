from django import forms

from .models import Proob


class ProobForm(forms.ModelForm):
    
    class Meta:
        model = Proob
        fields = [
            'title',
            'sub_title',
            'quanti',
        ]
        widgets = {
            'title':forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese el texto aquí.'
                }
            )
        } 


    def clean_quanti(self):
        data = self.cleaned_data["quanti"]
        if data < 10:
            raise forms.ValidationError("Debe ser mayor a 10")
        return data
    