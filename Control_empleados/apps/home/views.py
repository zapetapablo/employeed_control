from django.shortcuts import render
#
from django.views.generic import (
    TemplateView,
    ListView, 
    CreateView,
    )
from .models import Proob
from .forms import ProobForm

class view_proob(TemplateView):
    template_name = 'home/index.html'

class foundation_view(TemplateView):
    template_name = 'home/foundation.html'

class PruebaListView(ListView):
    # model = Prueba  aca irira un modelo para listar
    template_name = "home/list.html"
    context_object_name = 'num_list'
    queryset = ['0', '1', '2', '3', '4', '5',]


class Proob_list(ListView):
    template_name = "home/list_proob.html"
    model = Proob
    context_object_name = 'list'

class ProobCreateView(CreateView):
    model = Proob
    template_name = "home/home.html"
    # fields = ['title', 'sub_title', 'quanti']
    form_class = ProobForm
    success_url = '/'

