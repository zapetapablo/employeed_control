from django.contrib import admin
from django.urls import path

# from .views import view_proob, ListView
from . import views #import all class of views.py

app_name = "home_app"

urlpatterns = [
    path('proob/', views.view_proob.as_view()),
    path('lista/', views.PruebaListView.as_view()),
    path('pruebalista', views.Proob_list.as_view()),
    path('home', views.ProobCreateView.as_view(), name='pruebadd'),
    path('foundation',
        views.foundation_view.as_view(),
        name='foundation'
    ),
]
